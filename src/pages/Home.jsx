import React from 'react'
import ProductListHome from '../containers/ProductListHome/ProductListHome';

const Home = () => {
    return (
        <React.Fragment>
            <div className="container">
                <div className="row">
                    <ProductListHome/>
                </div>
            </div>
        </React.Fragment>

    )
}

export default Home;
