import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { login } from '../actions/user.action'
import { useFormik } from "formik"
import * as Yup from "yup"
import "./Login.css"


var logo = require("../img/logoHeader1.png")

function Login() {
    const dispatch = useDispatch()
    const history = useHistory()
    const auth = useSelector(state => state.auth)
    
    const formik = useFormik({
        initialValues: auth,
        enableReinitialize: true,
        validationSchema: Yup.object({
            username: Yup.string()
                .required("Trường này không được để trống"),
            password: Yup.string()
                .required("Trường này không được để trống"),
        }),
        onSubmit: data => {
            localStorage.setItem("password",data.password);
            dispatch(login(data.username, data.password, history))
        }
    })

    return (
        <div className='container-fluid wrap'>
            <div className="row">
                <div className='tittle col-md-6' >
                    <img src={logo} alt="logo" />
                    <h1>Luôn tươi ngon mỗi ngày <br />Thêm dinh dưỡng cho bữa ăn của bạn</h1>
                </div>
                <form onSubmit={formik.handleSubmit} className="col-md-6 login">
                    <div className='row'>
                        <div className='form-inner col-md-12'>
                            <h2>Login Form</h2>
                            <div className="form-group">
                                <label htmlFor="username">Username:</label>
                                <input className="form-control" type="text" id="username" name="username" onChange={formik.handleChange} />
                                {formik.errors.username && formik.touched.username && (
                                    <span className="error">{formik.errors.username}</span>
                                )}
                            </div>
                            <div className="form-group">
                                <label htmlFor="password" >Password:</label>
                                <input className="form-control" type="password" name="password" id="password" onChange={formik.handleChange} />
                                {formik.errors.password && formik.touched.password && (
                                    <span className="error">{formik.errors.password}</span>
                                )}
                            </div>
                            <a className="forgot"href="">Forgot your password?</a>
                            <div className="clear"></div>
                            <div className="btnsubmit">
                                <input type="submit" value="LOGIN" />
                            </div>
                        </div>
                    </div>



                </form>
            </div>
        </div>
    )
}

export default Login;