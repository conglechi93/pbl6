import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { register } from '../actions/user.action'
import { useFormik } from "formik"
import * as Yup from "yup"
import "./Register.css"

var logo = require("../img/logoHeader1.png")
function Register() {
    const dispatch = useDispatch()
    const history = useHistory()
    const auth = useSelector(state => state.auth)

    const formik = useFormik({
        initialValues: auth,
        enableReinitialize: true,
        validationSchema: Yup.object({
            username: Yup.string()
                .required("Trường này không được để trống"),
            password: Yup.string()
                .required("Trường này không được để trống"),
            fullName: Yup.string()
                .required("Trường này không được để trống"),
            phone: Yup.string()
                .required("Trường này không được để trống"),
            gender: Yup.string()
                .required("Trường này không được để trống"),
        }),
        onSubmit: data => {
            dispatch(register(data, history))
        }
    })

    return (
        <div className="container-fluid wrap">
            <div className="row">
                <div className='tittle col-md-6' >
                    <img src={logo} alt="logo" />
                    <h1>Luôn tươi ngon mỗi ngày <br />Thêm dinh dưỡng cho bữa ăn của bạn</h1>
                </div>
                <form onSubmit={formik.handleSubmit} className="col-md-6 m-auto register">
                    <div className='row'>
                        <div className='form-inner col-md-12'>
                            <h2>Register Form</h2>
                            <div className="form-group">
                                <label htmlFor="username">Username:</label>
                                <input className="form-control" type="text" id="username" name="username" value={formik.values.username} onChange={formik.handleChange} />
                                {formik.errors.username && formik.touched.username && (
                                    <span className="error">{formik.errors.username}</span>
                                )}
                            </div>

                            <div className="form-group">
                                <label htmlFor="fullName">Full Name:</label>
                                <input className="form-control" type="text" name="fullName" id="fullName" value={formik.values.fullName} onChange={formik.handleChange} />
                                {formik.errors.fullName && formik.touched.fullName && (
                                    <span className="error">{formik.errors.fullName}</span>
                                )}
                            </div>
                            <div className="form-group">
                                <label htmlFor="phone">Phone:</label>
                                <input className="form-control" type="text" name="phone" id="phone" value={formik.values.phone} onChange={formik.handleChange} />
                                {formik.errors.phone && formik.touched.phone && (
                                    <span className="error">{formik.errors.phone}</span>
                                )}
                            </div>
                            <div className="form-group">
                                <div className="radiogender">
                                    <p htmlFor="gender">Gender:</p>
                                    <div className="radioChild">

                                        <input type="radio" value="male" name="gender" onChange={formik.handleChange} />
                                        <label className="mr-2">Male</label>
                                    </div>
                                    <div className="radioChild">

                                        <input type="radio" value="female" name="gender" onChange={formik.handleChange} />
                                        <p>Female</p>
                                    </div>
                                </div>
                                {formik.errors.gender && formik.touched.gender && (
                                    <span className="error">{formik.errors.gender}</span>
                                )}
                            </div>
                            <div className="form-group">
                                <label htmlFor="password" >Password:</label>
                                <input className="form-control" type="password" name="password" id="password" value={formik.values.password} onChange={formik.handleChange} />
                                {formik.errors.password && formik.touched.password && (
                                    <span className="error">{formik.errors.password}</span>
                                )}
                            </div>



                            <div>Are you ready account?
                                <Link to="/login">Login</Link>
                            </div>

                            <div className="btnsubmit">
                                <input type="submit" value="REGISTER" />
                            </div>
                        </div>

                    </div>
                </form >
            </div>
        </div >

    )
}

export default Register;