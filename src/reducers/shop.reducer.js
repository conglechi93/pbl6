import {
    ADD_PRODUCT_TO_CART,
    DECREMENT_CART_ITEM_QUANTITY,
    INCREMENT_CART_ITEM_QUANTITY,
    REMOVE_PRODUCT_FROM_CART
} from '../actions';
import { productConstants } from '../_constants/product.constants';

const initialState = {
    products: [],
    shopDetail: null,
    categories:[],
    shops:[],
    cart: [],
    numberCart: 0,
    totalCart: 0,
};
const shopReducer = (state = initialState, action ) => {
    let updatedCart;
    let updatedItemIndex;

    switch (action.type) {
        case productConstants.SET_PRODUCTS:
            return {
                ...state,
                products: action.payload
            }
        case productConstants.SET_SHOP:
            return {
                ...state,
                shopDetail: action.payload
            }
        case productConstants.GET_SHOPS:
            return {
                ...state,
                shops: action.payload
        }        
        case productConstants.SET_CATEGORIES:
            return {
                ...state,
                categories: action.payload
            }
        case productConstants.ADD_CATEGORY:       
            return {
                state
            };

        case productConstants.DELETE_CATEGORY:
            const id = action.payload
            const newCategories = state.categories.filter(item => item._id !== id)
            console.log(newCategories)
            
            return {
                ...state,
                categories:newCategories
            };
        case productConstants.UPDATE_PRODUCT_FROM_CART:
            let _cart = [...state.cart];
            let _numberCart = 0;
            let _totalCart = 0;

            if (_cart) {
                _cart = JSON.parse(localStorage.getItem('cart'))
            }
            _cart.map((item, key) => {
                if (item._id == action.data.productId) {
                    _cart[key].quantity = parseInt(action.data.quantity);
                }
            })

            _cart.map(item => {
                _numberCart += parseInt(item.quantity);
                _totalCart += parseInt(item.quantity) * item.price;
            })

            localStorage.setItem('cart', JSON.stringify(_cart))
            localStorage.setItem('numberCart', _numberCart)
            localStorage.setItem('totalCart', _totalCart)
            return {
                ...state,
                cart: _cart,
                numberCart: _numberCart,
                totalCart: _totalCart
            }
        case productConstants.ADD_TO_CART:
            let cart = [...state.cart];
            let product = action.payload.product;
            let quantity = action.payload.quantity;
            let numberCart = 0;
            let totalCart = 0;

            let existCart = localStorage.getItem('cart', []);
            if (existCart) {
                cart = JSON.parse(existCart);
            }

            const productItem = {
                ...product,
                quantity: quantity
            }

            if (cart) {
                let flag = false;
                cart.map((item,key)=>{
                    if(item._id == product._id){
                        cart[key].quantity += parseInt(quantity);
                        flag=true;
                    }
                });
                if(!flag){
                    cart.push(productItem);
                }
            } else {
                cart = [];
                cart.push(productItem)
            }

            cart.map(item => {
                numberCart += parseInt(item.quantity);
                totalCart += parseInt(item.quantity) * item.price;
            })

            localStorage.setItem('cart', JSON.stringify(cart))
            localStorage.setItem('numberCart', numberCart)
            localStorage.setItem('totalCart', totalCart)
            
            return {
                ...state,
                cart: cart,
                numberCart: numberCart,
                totalCart: totalCart
            }
        case productConstants.REMOVE_PRODUCT_FROM_CART:
            let cart01 = [...state.cart];
            let numberCart01 = 0;
            let totalCart01 = 0;
            if (cart01) {
                cart01 = JSON.parse(localStorage.getItem('cart'))
            }
            cart01 = cart01.filter((item, key) => {
                return item._id != action.productId
            })
            
            cart01.map(item => {
                numberCart01 += parseInt(item.quantity);
                totalCart01 += parseInt(item.quantity) * item.price;
            })

            localStorage.setItem('cart', JSON.stringify(cart01))
            localStorage.setItem('numberCart', numberCart01)
            localStorage.setItem('totalCart', totalCart01)
            return {
                ...state,
                cart: cart01,
                numberCart: numberCart01,
                totalCart: totalCart01
            }
        case productConstants.SAVE_PRODUCT_TO_CART:
            console.log(action.payload)
            return {
                ...state
            }
        
        case INCREMENT_CART_ITEM_QUANTITY:
            updatedCart = [...state.cart];

            updatedItemIndex = updatedCart.findIndex(
                item => item.id === action.payload
            );

            const incrementedItem = {
                ...updatedCart[updatedItemIndex]
            };

            incrementedItem.quantity++;

            updatedCart[updatedItemIndex] = incrementedItem;


            return {...state, cart: updatedCart};

        case DECREMENT_CART_ITEM_QUANTITY:
            updatedCart = [...state.cart];
            updatedItemIndex = updatedCart.findIndex(
                item => item.id === action.payload
            );

            const decrementedItem = {
                ...updatedCart[updatedItemIndex]
            };

            decrementedItem.quantity--;

            updatedCart[updatedItemIndex] = decrementedItem;

            return {...state, cart: updatedCart};

        case ADD_PRODUCT_TO_CART:
            updatedCart = [...state.cart];
            updatedItemIndex = updatedCart.findIndex(item => item.id === action.payload.id);

            if(updatedItemIndex < 0) {
                updatedCart.push({...action.payload, quantity: 1});
            } else {
                const updatedItem = {
                    ...updatedCart[updatedItemIndex]
                };

                updatedItem.quantity++;
                updatedCart[updatedItemIndex] = updatedItem;
            }

            return {...state, cart: updatedCart};
        case REMOVE_PRODUCT_FROM_CART:
            updatedCart = [...state.cart];
            updatedItemIndex = updatedCart.findIndex(
                item => item.id === action.payload
            );

            updatedCart.splice(updatedItemIndex, 1);

            return {...state, cart: updatedCart};
        default:
            return state;

    }
};

export default shopReducer;