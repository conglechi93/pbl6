import { combineReducers } from "redux"
import authentication from "./auth.reducer";
import shopReducer from "./shop.reducer";

const rootReducer = combineReducers({
    auth: authentication,
    shop: shopReducer,
});

export default rootReducer;