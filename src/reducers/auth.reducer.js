import {userConstants } from '../_constants/user.constants';
let user = localStorage.getItem('user');

const initialState = {
  username: '',
  password: '',
  fullName: '',
  phone: '',
  gender: '',
  error: '',
  isLogged: user ? true : false,
  user: null,
  infoUser: ''
}

const authentication = (state = initialState, action) =>{
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user
      };

    case userConstants.LOGIN_SUCCESS:
      localStorage.setItem('user', action.payload.user)
      localStorage.setItem('infoUser', JSON.stringify(action.payload.infoUser))
      return {
        ...state,
        isLogged: true,
        user: action.payload.user,
        infoUser: action.payload.infoUser
      };
      
    case userConstants.LOGIN_FAILURE:
      return {};

    case userConstants.LOGOUT:
      localStorage.removeItem('user')
      localStorage.removeItem('infoUser')
      localStorage.removeItem('cart')
      localStorage.removeItem('productDetail')
      localStorage.removeItem('totalCart')
      localStorage.removeItem('numberCart')
      return {
        ...state,
        isLogged: false
      };

    default:
      return state
  }
}

export default authentication;