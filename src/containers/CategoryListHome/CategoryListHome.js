import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getAllCategory } from '../../actions/shop.action'
import CategoryList from '../../components/CategoryList/CategoryList';


function CategoryListHome(props) {

    const categories = useSelector(state => state.shop.categories);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getAllCategory())
    }, [])
    return (
        <div className="wrap_product">
            <input type="text" />
            <button>Search</button>
            <div className=" AllProduct__title">
                <h6 className="email"></h6>
                <h6 className="fullname">Tên loại</h6>
                <h6 className='phone'>Image</h6>
                <h6>Xóa</h6>
                <h6>Sửa</h6>
            </div>
            {categories && categories.map((category, index) => {
                return (
                    <CategoryList key={index} category={category} />
                )
            })}
        </div>

    );
}

export default CategoryListHome;
