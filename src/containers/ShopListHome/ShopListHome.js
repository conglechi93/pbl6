import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getAllShop } from '../../actions/shop.action'

function ProductListHome(props) {
    const shops = useSelector(state => state.shop.shops)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getAllShop())
    }, [])

    console.log(shops)

    return (
        <div className="col-lg-12 list-product">
            <h3 className="m-2">HighLights</h3>
            <div className="row">
                {/* {products.map((product, index) =>{                     
                    return (<div className="col-lg-2 col-md-4 col-sm-6 col-6" key={index}>
                    <ProductList key={index} product={product} />
                </div>)
                })}                                  */}
            </div>
        </div>
    );
}

export default ProductListHome;