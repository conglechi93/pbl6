import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getAllProduct } from '../../actions/shop.action'
import ProductList from '../../components/ProductList/ProductList'
import {paginationPipe} from '../../pines/paginationFilter'
import Catalogue from '../../components/Catalogue/Catalogue';
import ReactPaginate from 'react-paginate'

function ProductListHome({currentItems}) {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getAllProduct())
    }, [])

    return (
        <>
            <div>
                <Catalogue/>
            </div>
            <div className="col-lg-12 list-product">
                <h3 className="m-2">HighLights</h3>
                <div className="row">
                    {currentItems && currentItems.map((product, index) =>{                     
                        return (<div className="col-lg-3 col-md-4 col-sm-6 col-6" key={index}>
                        <ProductList key={index} product={product} />
                    </div>)
                    })}                                 
                </div>
            </div>
        </>

    );
}

function PaginatedItems({ itemsPerPage = 8 }) {
    const products = useSelector(state => state.shop.products)
    const [currentItems, setCurrentItems] = useState(null);
    const [pageCount, setPageCount] = useState(0);
    const [itemOffset, setItemOffset] = useState(0);
  
    useEffect(() => {
        const endOffset = itemOffset + itemsPerPage;
        setCurrentItems(products.slice(itemOffset, endOffset));
        setPageCount(Math.ceil(products.length / itemsPerPage));
    }, [products, itemOffset, itemsPerPage]);
  
    const handlePageClick = (event) => {
        const newOffset = (event.selected * itemsPerPage) % products.length;
        setItemOffset(newOffset);
    };
  
    return (
        <>
            <ProductListHome currentItems={currentItems} />
            <div className="pagination">
                <ReactPaginate
                    breakLabel="..."
                    nextLabel=">"
                    onPageChange={handlePageClick}
                    pageRangeDisplayed={5}
                    pageCount={pageCount}
                    previousLabel="<"
                    renderOnZeroPageCount={null}
                />
            </div>
        </>
    );
  }

export default PaginatedItems;