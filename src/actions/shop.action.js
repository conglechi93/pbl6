import axios from 'axios';
import toast from 'react-hot-toast';
import ApiService from '../_api';
import { productConstants } from '../_constants/product.constants';
import reqAxios from '../_constants/axios.constants'

export const getAllProduct = () => {
    return dispatch => {
        reqAxios()
            .get('/product')
            .then(response => {
                if (response.status === 200) {
                    dispatch(setProducts(response.data))
                }        
            })
            .catch(error => {
              console.log(error)
            });
    };
}

export const getAllCategory = () => {
    return dispatch => {
        reqAxios()
            .get('/category')
            .then(response => {
                if (response.status === 200) {
                    dispatch(setCategories(response.data))
                }        
            })
            .catch(error => {
              console.log(error)
            });
    };
}

export const deleteCategory = (id) => {
    console.log(id)
    return dispatch => {
        reqAxios()
            .delete(`/category/${id}`)
            .then(response => {
                if (response.status === 200) {
                    dispatch(_deleteCategory(id))
                }        
            })
            .catch(error => {
              console.log(error)
            });
    };
}

export const _deleteCategory = (payload) => {
    return {
        type: productConstants.DELETE_CATEGORY,
        payload
    }
}



export const getAllShop = () => {
    return dispatch => {
        reqAxios()
            .get('/shop')
            .then(response => {
                if (response.status === 200) {
                    dispatch(getShops(response.data))
                }        
            })
            .catch(error => {
              console.log(error)
            });
    };
}

export const addProduct = (data) => {
    return dispatch => {
        reqAxios()
        .post('product/add', data)
        .then(response => {
            if (response.status === 200) {
                dispatch(_addProduct(response.data))
            }    
        })
        .catch(error => {
            console.log(error)
        });
    };
}
export const _addProduct = () => {

}

export const showShop = (shopId) => {
    return dispatch => {
        reqAxios()
          .get('/shop/' + shopId)
          .then(response => {
              if (response.status === 200) {
                dispatch(setShop(response.data)) 
              }
          })
          .catch(error => {
            console.log(error)
          });
  };
}

export const addCategory = (data,history) => {
    return dispatch => {
        reqAxios()
        .post('/category/add', data)
        .then(response => {
            if (response.status === 200) {
                dispatch(_addCategory(response.data))
                history.push("/admin")
                // history.push("/admin/manage")
            }    
        })
        .catch(error => {
            console.log(error)
        });
    };
}

export const _addCategory = (payload) => {
    return {
        type: productConstants.ADD_CATEGORY,
        payload
    }
}

export const addToCart = (product, quantity) => {
    return dispatch => {
        const data = {
            product: product,
            quantity: quantity,
        }
        toast.success('Thêm sản phẩm thành công!')
        dispatch(setCart(data))
    }
}

export const updateProductFromCart = (productId, quantity) => {
    const data = {
        productId: productId,
        quantity: quantity,
    }

    return {
        type: productConstants.UPDATE_PRODUCT_FROM_CART,
        data
    }
}

export const saveProductToCart = (cartId, cart, history) => {
    return dispatch => {
        cart.map(item => {
            let result = {
                cartId: cartId,
                productId: item._id,
                quantity: item.quantity
            }
            reqAxios().post('/cart/item/add', result).then(response => {
                if (response.status === 200) {
                    toast.success('Đặt hàng thành công!')
                    dispatch(clearCart())
                    history.push('/cart')
                }
            })
            .catch(error => {
                console.log(error)
            })
        })
    }
}


export const removeProductFromCart = (productId) => {
    return {
        type: productConstants.REMOVE_PRODUCT_FROM_CART,
        productId
    }
}

export const setCart = (payload) => {
    return {
        type: productConstants.ADD_TO_CART,
        payload
    }
}

export const setShop = (payload) => {
    return {
        type: productConstants.SET_SHOP,
        payload
    }
}


export const setProducts = (payload) => {
    return {
        type: productConstants.SET_PRODUCTS,
        payload
    }
}


export const setCategories = (payload) => {
    return {
        type: productConstants.SET_CATEGORIES,
        payload
    }
}

export const getShops = (payload) => {
    return {
        type: productConstants.GET_SHOPS,
        payload
    }
}

export const clearCart = () => {
    localStorage.removeItem('cart');
    localStorage.removeItem('numberCart');
    localStorage.removeItem('totalCart');
}