import {
  userConstants
} from '../_constants/user.constants'
import toast from 'react-hot-toast';
import reqAxios from '../_constants/axios.constants'

export const login = (username, password, history) => {
  return dispatch => {
    reqAxios()
      .post('/user/login', {
        username: username,
        password: password
      })
      .then(response => {
        if (response.status === 200) {
          let userId = JSON.parse(atob(response.data.split('.')[1]))._id;
          console.log(userId)
          const data = {
            user: response.data,
            infoUser: JSON.parse(atob(response.data.split('.')[1]))
          }
          dispatch(loginSuccess(data))
          history.push('/home')
          toast.success('Đăng nhập thành công!')
        }        
      })
      .catch(error => {
        console.log(error)
        toast.error('Đăng nhập thất bại!')
      });
  };
}

export const loginSuccess = (payload) => {
  return {
    type: userConstants.LOGIN_SUCCESS,
    payload
  }
}

export const logout = (history) => {
  history.push('/login')
  return {
    type: userConstants.LOGOUT,
  }
}

export const register = (data, history) => {
  return dispatch => {
    reqAxios()
      .post('/user/register', data)
      .then(response => {
        console.log(response)
        if (response.status === 200) {
          history.push('/login')
          toast.success('Đăng ký thành công!')
        }        
      })
      .catch(error => {
        console.log(error)
        toast.error('Đăng ký thất bại!')
      });
  };
}

// function getAll() {
//   return dispatch => {
//       dispatch(request());

//       userService.getAll()
//           .then(
//               users => dispatch(success(users)),
//               error => dispatch(failure(error.toString()))
//           );
//   };

//   function request() { return { type: userConstants.GETALL_REQUEST } }
//   function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
//   function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
// }
// //my
// function getID(){
//   return dispath=>{
//       dispath(request())
//       userService.getById()
//       .then(
//           users=>dispath(success(users)),
//           error=>dispath(failure(error.toString()))
//       );
//   }
//   function request() { return { type: userConstants.GETID_REQUEST } }
//   function success(users) { return { type: userConstants.GETID_SUCCESS, users } }
//   function failure(error) { return { type: userConstants.GETID_FAILURE, error } }
// }

// // prefixed function name with underscore because delete is a reserved word in javascript
// function _delete(id) {
//   return dispatch => {
//       dispatch(request(id));

//       userService.delete(id)
//           .then(
//               user => dispatch(success(id)),
//               error => dispatch(failure(id, error.toString()))
//           );
//   };

//   function request(id) { return { type: userConstants.DELETE_REQUEST, id } }
//   function success(id) { return { type: userConstants.DELETE_SUCCESS, id } }
//   function failure(id, error) { return { type: userConstants.DELETE_FAILURE, id, error } }
// }