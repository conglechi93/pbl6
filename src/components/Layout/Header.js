import React from 'react';

import {Link} from 'react-router-dom'

function Header(props) {
    return (
        <nav className="navbar navbar-expand-sm navbar-dark bg-primary mb-4">
            <div className="containner">
                <button className="navbar-toggle" type="button" data-toggle="collapse" data-target="#mobile-nav">
                    <span className="nav-bar-toggle-icon"/>
                </button>

                <div className="collapase navbar-collapse" id="m">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <a className="nav-link" href="/home">
                                Home
                            </a>
                        </li>
                    </ul>

                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <a className="nav-link" href="/home">
                                Home
                            </a>
                        </li>
                    </ul>
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <a className="nav-link" href="/home">
                                Home
                            </a>
                        </li>
                    </ul>

                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <Link to="/login" className="login">
                                Login
                            </Link>
                        </li>
                    </ul>

                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <a className="nav-link" href="/home">
                                Logout
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
}

export default Header;