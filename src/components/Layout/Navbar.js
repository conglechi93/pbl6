import React from 'react';

const Navbar = () => {
  return (
    <div></div>
  );
};


window.onscroll = function () { scrollFunction() };

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("header").style.boxShadow = "0px 1px 40px 0px rgb(0 0 0 / 10%)";
    document.getElementById("header").style.padding = "10px 2%";
    document.getElementById("header").style.background = "#fff";

  } else {
    document.getElementById("header").style.boxShadow = "0 0 0 0px #fff";
    document.getElementById("header").style.padding = "50px 2%";
    document.getElementById("header").style.background = "none";



  }
}
export default Navbar;