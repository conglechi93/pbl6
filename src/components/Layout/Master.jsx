import React from "react";
import { Provider } from 'react-redux'
import store from "../../store";
import { BrowserRouter, Route, Switch } from "react-router-dom"
import Header from './Header'
import Navbar from './Navbar'
import { routes } from '../../routes/index'

const Master = () => {
    return (
        <Provider store={store}>
            <React.Fragment>
                <BrowserRouter>
                    {/* <Header />  */}
                    <Switch>
                        {routes.map((route) => (
                            route.routeChilds.map((routeChild) => (
                            <Route
                                path={routeChild.path}
                                key={`route02-${routeChild.path}`}
                                exact={routeChild.exact}
                            >
                                {routeChild.component}
                            </Route>
                            ))

                        ))}
                    </Switch> 
                    {/* <Navbar /> */}
                </BrowserRouter>
            </React.Fragment>
        </Provider>
    )
}

export default Master