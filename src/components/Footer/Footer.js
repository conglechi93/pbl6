import React from 'react'
import { SocialIcon } from 'react-social-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone, faEnvelope, faLocationArrow, faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faInstagram, faTwitter } from "@fortawesome/free-brands-svg-icons";
import BackToTop from '../BackToTop/BackToTop';
import "./Footer.css"

const iconPhone = <FontAwesomeIcon icon={faPhone} />
const iconEnvelope = <FontAwesomeIcon icon={faEnvelope} />
const iconLocation = <FontAwesomeIcon icon={faLocationArrow} />
const iconAngleRight = <FontAwesomeIcon icon={faAngleRight} />
const iconFacebook = <FontAwesomeIcon icon={faFacebook} />
const iconInstagram = <FontAwesomeIcon icon={faInstagram} />
const iconTwitter = <FontAwesomeIcon icon={faTwitter} />


var img = require("../../img/logoHeader1.png");
var zalo = require("../../img/zalo_pay.png");
var momo = require("../../img/momo.png");
var jcb = require("../../img/jcb.png");
var visa = require("../../img/visa1.png");
var viettel = require("../../img/viettelpay.png");
var pay = require("../../img/pay.png");




const Footer = () => {
    return (
        <footer >
            <div className="footer__container container-fluid">
                <div className="footer__main row">
                    <div className="footer__info col-lg-3 col-md-6 ">
                        <img src={img} alt=""></img>
                        <div className="footer__contact--parent">
                            <div className="footer__contact">
                                <a href="#">{iconPhone}</a>
                                <div className="footer__phonenumber">
                                    <p>1800-123-4567</p>
                                </div>
                            </div>

                            <div className="footer__contact">
                                <a href="#">{iconLocation}</a>
                                <div className="footer__mail--detail">
                                    <p> Da Nang, VietNam</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="footer__link footer__before col-md-6 col-lg-3">
                        <h1>Our links</h1>
                        <ul>
                            <li><a href="#">
                                {iconAngleRight}
                                Home</a>
                            </li>
                            <li><a href="#">{iconAngleRight}
                                About Us</a></li>
                            <li><a href="#">{iconAngleRight}Services</a></li>
                            <li><a href="#">{iconAngleRight}Team</a></li>
                         
                        </ul>
                    </div>
                    <div className="footer__connect footer__before col-md-6 col-lg-3">
                        <h1>Connect With Us</h1>
                        <div className="footer__social">
                            <a href="#">{iconFacebook}</a>
                            <a href="#">{iconInstagram}</a>
                            <a href="#">{iconTwitter}</a>
                        </div>
                    </div>
                    <div className="footer__connect footer__before col-md-6 col-lg-3">
                        <h1>Payment Methods</h1>
                        <div className="footer__payment">
                            <img src={zalo} alt=""></img>
                            <img src={momo} alt=""></img>
                            <img src={viettel} alt=""></img>
                            <img src={jcb} alt=""></img>
                            <img src={visa} alt=""></img>
                            <img src={pay} alt=""></img>
                        </div>
                    </div>
                </div>

            </div>
            <div className="footer__bottom">
                <div className="footer__bottom--wrap">
                    <p>Copyright © 2021 <a href="#">DexignZone</a>.All rights reserved.
                    </p>

                </div>
                <div className="footer__backtotop">
                    <BackToTop />
                </div>
            </div>

        </footer>
    )
}

export default Footer
