import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { formatMoney } from '../../pines/priceFormatter'
import "./ProductList.css"

function ProductList(props) {
    const {
        title,
        price,
        images,
        date,
        description,
        id,
    } = props.product;
    const product = props.product;
    const imageRef = React.createRef();
    const [img, setImg] = useState("images[0]");
    const [aItem, setAItem] = useState(0);

    return (
        <Link to={`/products/${product._id}`} className="product__link">
            <div className="product-item">
                <div className="product__img">

                <img className="card-img-top " src={product.image} alt={product.name} />
                </div>

                <div className="down-content">
                    <h4 className="card-title product__title">
                        {product.name}
                    </h4>
                    <h6 className="product__price">{formatMoney(price)}đ</h6>


                </div>
            </div>
        </Link>
    );
};

export default ProductList;