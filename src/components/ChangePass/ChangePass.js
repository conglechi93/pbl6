import React from 'react';
import './ChangePass.css'
import { useState } from 'react';
const ChangePass = props => {
    const [passCurrent,setPassCurrent] = useState('')
    const [newPassword,setNewPassword] = useState('')
    const [confirmPassword,setConfirmPassword] = useState('')
    const handleSubmitChangePass = (e) => {
        e.preventDefault();
        const passLocalStorage = localStorage.getItem("password")
        const form = {
            passCurrent,
            newPassword,
            confirmPassword,
        }
        console.log(passLocalStorage)
        if(passLocalStorage === passCurrent) {
            if(newPassword!== confirmPassword){
                alert("Mật khẩu xác nhận không trùng nhau")
            }
            else{
                //changePassword(newPassword);
            }
        }
        else{
            alert("Mật khẩu hiện tại không trùng khớp")
        }
        
    }
    return (
        <div className="tab-pane fade " id="v-pills-password" role="tabpanel" aria-labelledby="v-pills-password-tab">
        <div className='form-inner col-md-12'>
            <div className="form-group form-profile">
                <label htmlFor="pwcr">Mật khẩu hiện tại:</label>
                <input type="password" className="passCurrent" id="pwcr" value={passCurrent} onChange={(e) => setPassCurrent(e.target.value)}></input>
            </div>
            <div className="form-group form-profile">
                <label htmlFor="pwn">Mật khẩu mới:</label>
                <input type="password" className="newPassword" id="pwn" value={newPassword} onChange={(e) => setNewPassword(e.target.value)}></input>
            </div>
            <div className="form-group form-profile">
                <label htmlFor="cfpwn">Xác nhận mật khẩu mới:</label>
                <input type="password" className="confirmPassword" id="cfpwn" value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)}></input>
            </div>
        </div>
        <div className="btn">
            <input className="btnSubmit" type="submit" value="Xác nhận" onClick={handleSubmitChangePass}/>
        </div>
    </div>
    );
};


export default ChangePass;