import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { removeProductFromCart, saveProductToCart, updateProductFromCart } from '../../actions/shop.action';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { formatMoney } from '../../pines/priceFormatter';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { useHistory } from 'react-router';
import "./Cart.css"
const iconTrash = <FontAwesomeIcon icon={faTrashAlt} />


export default function Cart() {
    const dispatch = useDispatch()
    let cart = useSelector(state => state.shop.cart)
    let existCart = localStorage.getItem('cart', [])
    let totalCart = cart.totalCart;
    const cartId = JSON.parse(localStorage.getItem('infoUser')).cartId;
    const history = useHistory();

    if (existCart) {
        cart = JSON.parse(existCart);
        totalCart = JSON.parse(localStorage.getItem('totalCart'))
    }

    const changeQuantity = (productId, quantity) => {
        if (quantity) {
            dispatch(updateProductFromCart(productId, quantity))
        }
    }

    const checkout = () => {
        dispatch(saveProductToCart(cartId, cart, history))
    }

    return (
        <div className="container-fluid cart-page">
            <div className="row">
                <div className="col-md-12">
                    <h3 className="mt-2">My Cart</h3>
                </div>
                <div className="col-md-12">
                    <table className="table table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                cart.length > 0 && cart.map((product, index) => {
                                    return (
                                        <tr key={index}>
                                            <td className="text-center">
                                                <a  className="btn btndelete " href='' onClick={() => dispatch(removeProductFromCart(product._id))}>
                                                    {iconTrash}
                                                </a>
                                            </td>
                                            <td>
                                                <div className="image1">
                                                    <img src={product.image} alt={product.image} />
                                                </div>
                                            </td>
                                            <td>
                                                <Link className='cart__title' to={`/products/${product._id}`}>{product.name}</Link>
                                            </td>
                                            <td>
                                                <input type="number" min="1" max="99" maxLength="2" value={product.quantity} onChange={(e) => changeQuantity(product._id, e.target.value)}/>
                                            </td>
                                            <td>{formatMoney(product.price)} VNĐ</td>
                                            <td className="td-total">
                                                <b>{formatMoney(product.quantity * product.price)} VNĐ</b>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                            {
                                cart.length > 0 ? 
                                <tr>
                                    <td colSpan="5" className="text-right">
                                        <b>Total</b>
                                    </td>
                                    <td>
                                        <b className='total'>{formatMoney(totalCart)} VNĐ</b>
                                    </td>
                                </tr> : 
                                <tr>
                                    <td colSpan="6" className="text-center">Your cart is empty.</td>
                                </tr>
                            }
                        </tbody>
                    </table>

                    <button className="btn btn-success " onClick={() => checkout()}>Checkout</button>
                </div>
            </div>
        </div>
    )
}
