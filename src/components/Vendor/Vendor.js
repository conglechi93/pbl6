import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';

import "./Vendor.css"
import ProductRegister from "../ProductRegister/AddProduct";
const iconTrash = <FontAwesomeIcon icon={faTrashAlt} />
const iconEdit = <FontAwesomeIcon icon={faEdit} />



const Vendor = () => {
    return (
        <div className="container shopmanagement">
            <div className="row">
                <div className="col-3">
                    <div className="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <h5>Quản lí sản phẩm</h5>
                        <a className="nav1 active nav-link--color" id="v-pills-allproduct-tab" data-toggle="pill" href="#v-pills-allproduct" role="tab" aria-controls="v-pills-allproduct" aria-selected="true">Tất cả sản phẩm</a>
                        <a className="nav1  nav-link--color" id="v-pills-addproduct-tab" data-toggle="pill" href="#v-pills-addproduct" role="tab" aria-controls="v-pills-addproduct" aria-selected="false">Thêm sản phẩm</a>
                        <h5>Quản lí đơn hàng</h5>
                        <a className="nav1  nav-link--color" id="v-pills-allshipment-tab" data-toggle="pill" href="#v-pills-allshipment" role="tab" aria-controls="v-pills-allshipment" aria-selected="false">Tất cả đơn hàng</a>
                        <a className="nav1  nav-link--color" id="v-pills-cancel-tab" data-toggle="pill" href="#v-pills-cancel" role="tab" aria-controls="v-pills-cancel" aria-selected="false">Đơn hủy</a>
                    </div>
                </div>
                <div className="col-9">
                    <div className="tab-content" id="v-pills-tabContent">
                        <div className="tab-pane fade show active" id="v-pills-allproduct" role="tabpanel" aria-labelledby="v-pills-allproduct-tab">
                            <div className=" AllProduct">
                                <div className=" AllProduct__title">
                                    <h6 className="email">Tên sản phẩm</h6>
                                    <h6 className="fullname">Giá</h6>
                                    <h6 className="phone">Đã bán</h6>
                                    <h6></h6>
                                    <h6></h6>
                                </div>
                                <div className=" AllProduct__content">
                                    <div className="AllProduct__item">
                                        <p className="email">Tôm hùm</p>
                                        <p className="fullname"> 1.000.000</p>
                                        <p className="phone">15</p>
                                        <a className="content__dele" href="">{iconTrash}Xóa</a>
                                        <Link target="_blank" to="/vendors/edit" className="content__edit" href="">{iconEdit}Sửa</Link>
                                    </div>




                                </div>

                            </div>


                        </div>
                        <div className="tab-pane fade" id="v-pills-addproduct" role="tabpanel" aria-labelledby="v-pills-addproduct-tab">
                            <ProductRegister />
                        </div>
                        <div className="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">

                        </div>
                        <div className="tab-pane fade" id="v-pills-allshipment" role="tabpanel" aria-labelledby="v-pills-allshipment-tab">
                            <div className=" AllProduct">
                                <div className=" AllProduct__title">
                                    <h6 className="email">Mã đơn hàng</h6>
                                    <h6 className="fullname">Sản phẩm</h6>
                                    <h6 className='phone'>Tổng tiền</h6>
                                </div>
                                <div className=" AllProduct__content">
                                    <a href="">
                                        <div className="AllProduct__item">
                                            <p className="email">0029109</p>
                                            <p className="fullname">Cua hoàng đế, Tôm hùm</p>
                                            <p className='phone'>3.000.000</p>


                                        </div>
                                    </a>

                                </div>
                            </div>

                        </div>
                        <div className="tab-pane fade" id="v-pills-cancel" role="tabpanel" aria-labelledby="v-pills-cancel-tab">

                        </div>
                    </div>
                </div>

            </div>

        </div>

    )
}

export default Vendor
