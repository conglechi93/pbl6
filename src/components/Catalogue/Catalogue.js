import React from 'react'
import { useState, useEffect } from 'react';
import "./Catalogue.css"
var request = "https://pbl6-seafood.herokuapp.com/api/category"

function Catalogue() {
    const [data, setData] = useState([]);
    useEffect(() => {
        fetch(request, { mode: 'cors' })
            .then(res => res.json())
            .then(data => {
                setData(data)
            })
            .catch(function (error) {
                console.log('Request failed', error)
            })
    }, [])
    return (
        <div className="catalogue">
            <div className="catalogue__wrap">
                <div className="catalogue__heading">
                    <h2>Catalogue</h2>
                </div>
                <div className="row ">
                    {data.map((item,index) => (
                        <div className="col-md-2 catalogue__item" key={index}>
                            <a href="">
                                <img src={item.image} alt="" />
                                <h4>{item.name}</h4>
                            </a>
                        </div>
                    ))}
                </div>
            </div>
        </div >
    )
}

export default Catalogue
