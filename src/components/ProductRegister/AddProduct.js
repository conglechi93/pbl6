import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { addProduct, getAllCategory, getAllShop } from '../../actions/shop.action';
import "./AddProduct.css"

var img = require("../../img/1.jpg")
var img2 = require("../../img/2.jpg")

const AddProduct = () => {
    const dispatch = useDispatch();
    const categories = useSelector(state => state.shop.categories)
    const shops = useSelector(state => state.shop.shops)
    const [nameProduct, setNameProduct] = useState("");
    const [categoryId, setCategory] = useState("");
    const [price, setPrice] = useState("");
    const [quantity, setQuantity] = useState("");
    const [image, setImage] = useState("");
    const [shopId, setShop] = useState("");
    const [description, setDescription] = useState("");


    useEffect(() => {
        dispatch(getAllCategory())
        dispatch(getAllShop())
    }, [])

    const onSubmitAddProduct = () => {
        const form = {
            nameProduct, categoryId, price, quantity, image, shopId, description
        }
        dispatch(addProduct(form))
    }

    return (
        <div className="login-box">
            <form onSubmit={onSubmitAddProduct}>
                <div className="wrap__main row">
                    <div className="image col-md-6">
                        <img className='img1' src={img} alt="" />
                        <img className='img2' src={img2} alt="" />
                    </div>
                    <div className="col-md-6">

                        <div className="user-box">
                            
                            <input type="text" name="name-product" value={nameProduct} onChange={e => setNameProduct(e.target.value)} />
                            <label>Tên sản phẩm</label>
                        </div>
                        <div className='user-mgg'>
                            <label>Loại sản phẩm</label> <br></br>
                        
                            <select className="user-box1" id="exampleFormControlSelect1" name="" onChange={e => setCategory(e.target.value)}>
                                {
                                    categories && categories.map((category, index) => {
                                        return (
                                            <option key={index} value={category._id}>{category.name}</option>
                                        )
                                    })
                                }
                            </select>
                        </div>
                 

                        <div className="user-box">
                            <input type="text" name="" value={price} onChange={e => setPrice(e.target.value)} />
                            <label>Giá sản phẩm</label>
                        </div>
                        <div className="user-box">
                            <input type="number" name="quantity" value={quantity} onChange={e => setQuantity(e.target.value)}></input>
                            <label>Số lượng</label>
                        </div>

                        <div className='user-box1 user-mg'>
                            <label htmlFor="">Hình ảnh sản phẩm</label> <br />
                            <input type="file" id="img" name="img" accept="image/*" value={image} onChange={e => setImage(e.target.value)}></input>

                        </div>
                        <div className="user-box1">
                            <label>Mô tả sản phẩm</label>
                            <textarea className="form-control" id="description" rows="3" value={description} onChange={e => setDescription(e.target.value)}></textarea>
                        </div>
                    </div>



                </div>
                <div className="btn btnAdd">
                    <input className="btnSubmit" type="submit" value="Thêm" />
                </div>
            </form>

        </div >
    )
}

export default AddProduct