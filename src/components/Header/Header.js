import React from 'react';
import { Link, useHistory } from 'react-router-dom'
import { Container, Nav, Navbar } from 'react-bootstrap'
import { useSelector, useDispatch } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearchPlus, faCartPlus, faUser } from '@fortawesome/free-solid-svg-icons';
import { useEffect } from 'react'
import { logout } from '../../actions/user.action';
import "./Header.css"
const iconSearch = <FontAwesomeIcon icon={faSearchPlus} />
const iconUser = <FontAwesomeIcon icon={faUser} />

var img = require("../../img/logoHeader1.png")

function Header(props) {
    const history = useHistory()
    const redirect = (url) => {
        history.push(url)
    }
    const dispatch = useDispatch()
    const auth = useSelector(state => state.auth)
    const infoUser = JSON.parse(localStorage.getItem('infoUser'))
    let numberCart = useSelector(state => state.shop.numberCart)
    let existNumberCart = localStorage.getItem('numberCart')
    if (existNumberCart) {
        numberCart = existNumberCart;
    }

    return (
        <header id="header">
            <Navbar className='container-fluid'>

                <Navbar.Brand href="#home">
                    <img className="nav__logo" src={img} alt="" />
                </Navbar.Brand>
                
                <Nav className="me-auto">
                    <Nav.Link onClick={() => redirect('/home')}>Home</Nav.Link>
                    <Nav.Link onClick={() => redirect('/shops')}>Shops</Nav.Link>
                    <div className="search-container">
                        <form action="">
                            <input type="text" placeholder="Search.." name="search"></input>
                            <button type="submit">{iconSearch}</button>
                        </form>
                    </div>
                    {
                        auth.isLogged === false ?
                            <>
                                {/* <Nav.Link onClick={() => redirect('shop')}>Shops</Nav.Link>
                                <div className="search-container">
                                    <form action="">
                                        <input type="text" placeholder="Search.." name="search"></input>
                                        <button type="submit">{iconSearch}</button>
                                    </form>
                                </div> */}
                                
                                <Nav.Link className="unnavlink btnInOut1" id="btnInOut" onClick={() => redirect('/register')}>Register</Nav.Link>
                                <Nav.Link className="unnavlink " id="btnInOut" onClick={() => redirect('/login')}>Login</Nav.Link>
                                {/* <Nav.Link className="iconCart unnavlink" onClick={() => redirect('/cart')}> 
                                    <div className="cart-header">
                                        <i iconCart className="fa fa-shopping-cart"></i>
                                        <span className="total-cart">{numberCart}</span>
                                    </div>
                                </Nav.Link>  */}
                            </> :
                            <>
                                <Nav.Link className="iconUser unnavlink" onClick={() => redirect('/profile')}>{iconUser} {infoUser ? infoUser.username : auth.infoUser.username}</Nav.Link>
                                <Nav.Link onClick={() => dispatch(logout(history))}>Logout</Nav.Link>
                            </>
                    }
                    <Nav.Link className="iconCart unnavlink" onClick={() => redirect('/cart')}> 
                        <div className="cart-header">
                            <i className="fa fa-shopping-cart"></i>
                            <span className="total-cart">{numberCart}</span>
                        </div>
                    </Nav.Link> 
                </Nav>

            </Navbar>
        </header>
    );
}

export default Header;