import { Redirect, Route } from "react-router";
import Home from "../../pages/Home";

export const grantPermission = (requestedRoles) => {
    const infoUser =  JSON.parse(localStorage.getItem('infoUser'));
    const role = infoUser.role;

    if (requestedRoles.includes(role)) {
        return true;
    }
    return false;
};

function RoleBasedRouting({
    component: Component, roles, ...rest
  }) {
    return (
      <>
        {roles}
        {grantPermission(roles) && (
            <Route
                {...rest}
                render={(props) => (
                    <>
                    <Component {...props} />
                    </>
                )}
            />
        )}
        {
          !grantPermission(roles) && (
            <Redirect to="/home" />
          )
        }
      </>
    );
}

export default RoleBasedRouting