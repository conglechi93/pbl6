import React from 'react'
import  { useEffect, useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowAltCircleUp} from '@fortawesome/free-solid-svg-icons';
const iconAngle = <FontAwesomeIcon icon={faArrowAltCircleUp} />
export default function BackToTop() {
    const [isVisible, setIsVisible] = useState(false);


    const toggleVisibility = () => {
        if (window.pageYOffset > 300) {
            setIsVisible(true);
        } else {
            setIsVisible(false);
        }
    };


    const scrollToTop = () => {
        window.scrollTo({
        top: 0,
        behavior: "smooth"
        });
    };

    useEffect(() => {
        window.addEventListener("scroll", toggleVisibility);
    }, []);

    return (
        <div className="scroll-to-top">
        {isVisible && 
            <div className="scrolltotop"onClick={scrollToTop}>
                <a href="">{iconAngle}</a>
            </div>}
        </div>
    );
}

