import React,{useState} from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import {addCategory,} from '../../actions/shop.action'
import { useHistory } from 'react-router';

function AddCategory(props) {
    const [nameCategory,setNameCategory] = useState("");
    const [imageCategory,setImageCategory] = useState("");
    const dispatch = useDispatch();
    const history = useHistory()
    const onSubmitAddCategory = () => {
        const form = {
            nameCategory,
            imageCategory,
        }
        console.log(form)
        dispatch(addCategory(form, history))
        
    }
    return (
    <div className="add-category-box">
        <form onSubmit={onSubmitAddCategory}>
            <div className="" >
                <div className="">
                    <label>Tên loại sản phẩm</label>
                    <input type="text" name="nameCategory" value={nameCategory} onChange={e=>setNameCategory(e.target.value)}/>
                </div>
                <div className="">
                    <label htmlFor="">Hình ảnh loại sản phẩm</label> <br />
                    <input type="file" id="img" name="imgCategory" accept="image/*" value={imageCategory} onChange={e=>setImageCategory(e.target.value)}></input>
                </div>
                    
            </div>
                <div className="form-group">
                    <div className="col-md-6 text-right">
                        <button type="button" className="btn btn-primary" onClick={() => onSubmitAddCategory()}>
                            Thêm
                        </button>
                    </div>
                </div> 
            </form>
        </div>
    );
}

export default AddCategory;