import React, {useState} from 'react'
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons';
import "./categorylist.css"
import {deleteCategory} from '../../actions/shop.action'
import { useSelector, useDispatch } from 'react-redux';

const iconTrash = <FontAwesomeIcon icon={faTrashAlt} />
const iconEdit = <FontAwesomeIcon icon={faEdit} />
const CategoryList = (props) => {
    const {
        _id,
        name,
        image
    } = props.category;
    const dispatch = useDispatch()
    const category = props.category;
    const imageRef = React.createRef();
    const [img, setImg] = useState("images[0]");
    const [aItem, setAItem] = useState(0);
    const handleDeleteCategory = (e) => {
        e.preventDefault();
        dispatch(deleteCategory(category._id))
    }
    return (

        <div>
            <div className=" AllProduct">
                <div className=" AllProduct__content">
                    <div className="AllProduct__item">
                        <p className="email">{category.id}</p>
                        <p className="fullname">{category.name}</p>
                        <img className='phone' src={category.image} alt="" />
                        <button className="content__dele" onClick={handleDeleteCategory}>{iconTrash}Xóa</button>
                        <Link target="_blank" to="/edit" className="content__edit" href="">{iconEdit}Sửa</Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CategoryList
