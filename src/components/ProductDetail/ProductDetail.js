import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router';
import { showShop, addToCart, getAllProduct } from '../../actions/shop.action';
import { formatMoney } from '../../pines/priceFormatter';
import "./ProductDetail.css"

export default function ProductDetail() {
    const dispatch = useDispatch();
    const shop = useSelector(state => state.shop);
    const [quantity, setQuantity] = useState(1)
    const { id } = useParams();

    useEffect(() => {
        if (shop.products.length < 1) {
            dispatch(getAllProduct())
        }
    }, [shop])

    let productDetail = shop.products.find(item => item._id === id);

    const shopId = productDetail ? productDetail.shop : ''
    useEffect(() => {
        if (shopId) {
            dispatch(showShop(shopId))
        }
    }, [shopId])

    return (
        <div className="container-fluid productDetail">
            <div className="productDetail__container">
                <div className="row">
                    <div className="col-md-12">
                        <h3 className="mt-2">Product Detail</h3>
                    </div>
                    {
                        productDetail &&
                        <>
                            <div className="productDetail--main row ">
                                <div className="col-md-5 product--detail--img">
                                    <img src={productDetail.image} className="" />
                                </div>
                                <div className="col-md-7 product-detail--wrap">

                                    <div className='product-detail--pd' >
                                        <h1>{productDetail.name}</h1>

                                        <p> {
                                            shop.shopDetail &&
                                            <div className="list">
                                                <a href="#">#{shop.shopDetail.name}</a>
                                            </div>
                                        }</p>

                                        <p className='des'>{productDetail.description}</p>
                                        <div className="productdetail__fl">
                                            <div className="table-responsive mb-2">
                                                <table className="table table-sm table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td className="pl-0 pb-0 w-25">Quantity</td>
                                                        </tr>
                                                        <tr>
                                                            <td className="pl-0">
                                                                <div className="def-number-input number-input safari_only mb-0">

                                                                    <input type="number" min="1" max="999" className="input-quantity" value={quantity} onChange={e => setQuantity(e.target.value)}></input>

                                                                </div>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div className="mybtn">
                                                <button className="btn">
                                                    <span className="price">{(productDetail.price)}đ</span>
                                                    <span className="shopping-cart"><i className="fa fa-shopping-cart" aria-hidden="true"></i></span>
                                                    <span className="buy" onClick={() => dispatch(addToCart(productDetail, quantity))}>Add to cart</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </>
                    }
                </div>

            </div>
        </div>
    )
}
