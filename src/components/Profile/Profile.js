import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faStoreAlt } from '@fortawesome/free-solid-svg-icons';
import "./Profile.css"
import ChangePass from '../ChangePass/ChangePass';
const iconEdit = <FontAwesomeIcon icon={faEdit} />
const iconStore = <FontAwesomeIcon icon={faStoreAlt} />

const Profile = () => {
    return (
        <div className="container profile">
            <div className="row">
                <div className="col-3">
                    <div className="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <h5>Tài khoản của tôi</h5>
                        <a className="nav1 active nav-link--color" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="true">Hồ sơ</a>
                        <a className="nav1  nav-link--color" id="v-pills-password-tab" data-toggle="pill" href="#v-pills-password" role="tab" aria-controls="v-pills-password" aria-selected="false">Đổi mật khẩu</a>
                        <h5>Đơn hàng của tôi</h5>
                        <a className="nav1 nav-link--color" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Tất cả</a>

                    </div>
                </div>
                <div className="col-9 form-pr">
                    <div className="tab-content" id="v-pills-tabContent">
                        <div className="tab-pane fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                            <div className='form-inner col-md-12'>

                                <div className="form-group form-profile">
                                    <label htmlFor="name">Họ tên:</label>
                                    <input className="form-control" type="text" id="name" name="name" value="Ngọc" />

                                </div>
                                <div className="form-group form-profile">
                                    <label htmlFor="email" >Email:</label>
                                    <p>luongngocjunsu@gmail.com</p>

                                </div>
                                <div className="form-group form-profile">
                                    <label htmlFor="phone" >Số điện thoại:</label>
                                    <input className="form-control" type="phone" name="phone" id="phone" value="0978831747" />

                                </div>
                                <div className="form-group form-profile">
                                    <label htmlFor="add">Địa chỉ:</label>
                                    <input className="form-control" type="add" name="add" id="add" value="Thôn 2, Tam Giang, Núi Thành, Quảng Nam" />

                                </div>

                            </div>
                            <div className="btn">
                                <input className="btnSubmit" type="submit" value="Xác nhận" />
                            </div>

                        </div>
                        <ChangePass/>
                        <div className="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                            <div className=" AllProduct">
                                <div>
                                    <p style={{ color: "#FF2829", fontWeight: "600" }}>{iconStore} QUEEN SEAFOOD</p>
                                </div>
                                <div className=" AllProduct__title">
                                    <h6 className="email">Tên sản phẩm</h6>
                                    <h6 className="fullname">Số lượng</h6>
                                    <h6 className='phone'>Giá</h6>

                                </div>
                                <div className=" AllProduct__content">
                                    <div className="AllProduct__item">
                                        <p className="email">Cua tuyết</p>
                                        <p className="fullname">2</p>
                                        <p className='phone' >2.000.000đ</p>

                                    </div>
                                    <div className="AllProduct__item">
                                        <p className="email">Tôm càng xanh</p>
                                        <p className="fullname">2</p>
                                        <p className='phone' >2.000.000đ</p>

                                    </div>
                                </div>
                                <div style={{float:"right",fontWeight:"600"}} className="total">
                                    <p>Total: 4.000.000đ</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Profile
