import React from 'react'
import NewUser from '../NewUser/NewUser'
import AddShop from '../AddShop/AddShop'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import CategoryListHome from "../../containers/CategoryListHome/CategoryListHome"
import AddCategory from '../AddCategory/AddCategory';
import "./AdminManagenment.css"

const iconTrash = <FontAwesomeIcon icon={faTrashAlt} />
const iconEdit = <FontAwesomeIcon icon={faEdit} />
const AdminManagement = () => {
    return (
        <div className="container shopmanagement">
            <div className="row">
                <div className="col-3">
                    <div className="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <h5>Quản lí người dùng</h5>
                        <a className="nav1 active nav-link--color" id="v-pills-alluser-tab" data-toggle="pill" href="#v-pills-alluser" role="tab" aria-controls="v-pills-alluser" aria-selected="true">Tất cả người dùng</a>
                        <a className="nav1 nav-link--color" id="v-pills-adduser-tab" data-toggle="pill" href="#v-pills-adduser" role="tab" aria-controls="v-pills-adduser" aria-selected="false">Thêm người dùng</a>
                        <h5>Quản lí shop</h5>
                        <a className="nav1 nav-link--color" id="v-pills-allshop-tab" data-toggle="pill" href="#v-pills-allshop" role="tab" aria-controls="v-pills-allshop" aria-selected="false">Tất cả shop</a>
                        <a className="nav1 nav-link--color" id="v-pills-addshop-tab" data-toggle="pill" href="#v-pills-addshop" role="tab" aria-controls="v-pills-addshop" aria-selected="false">Thêm shop</a>
                        <h5>Quản lí loại sản phẩm</h5>
                        <a className="nav1 nav-link--color" id="v-pills-allcategory-tab" data-toggle="pill" href="#v-pills-allcategory" role="tab" aria-controls="v-pills-allcategory" aria-selected="false">Tất cả loại sản phẩm</a>
                        <a className="nav1 nav-link--color" id="v-pills-addcategory-tab" data-toggle="pill" href="#v-pills-addcategory" role="tab" aria-controls="v-pills-addcategory" aria-selected="false">Thêm loại sản phẩm</a>
                    </div>
                </div>
                <div className="col-9">
                    <div className="tab-content" id="v-pills-tabContent">
                        <div className="tab-pane fade show active" id="v-pills-alluser" role="tabpanel" aria-labelledby="v-pills-alluser-tab">
                            <div className=" AllProduct">
                                <div className=" AllProduct__title">
                                    <h6 className="email">Email</h6>
                                    <h6 className="fullname">FullName</h6>
                                    <h6 className='phone'>Số điện thoại</h6>
                                    <h6></h6>
                                    <h6></h6>
                                </div>
                                <div className=" AllProduct__content">
                                    <div className="AllProduct__item">
                                        <p className="email">luongngocjunsu@gmail.com</p>
                                        <p className="fullname">Lương Ngọc</p>
                                        <p className='phone' >0978831747</p>
                                        <a className="content__dele" href="">{iconTrash}Xóa</a>
                                        <Link target="_blank" to="/ed" className="content__edit" href="">{iconEdit}Sửa</Link>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="tab-pane fade" id="v-pills-adduser" role="tabpanel" aria-labelledby="v-pills-adduser-tab">
                            <NewUser />
                        </div>
                        <div className="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">

                        </div>
                        <div className="tab-pane fade" id="v-pills-allshop" role="tabpanel" aria-labelledby="v-pills-allshop-tab">
                            <div className=" AllProduct">
                                <div className=" AllProduct__title">
                                    <h6 className="email">Tên shop</h6>
                                    <h6 className="fullname">Số điện thoại</h6>
                                    <h6 className='phone'>Địa chỉ</h6>
                                </div>
                                <div className=" AllProduct__content">
                                    <devicePixelRatio href="">
                                        <div className="AllProduct__item">
                                            <p className="email">QUEEN</p>
                                            <p className="fullname">0975744333</p>
                                            <p className='phone'>Quảng Nam</p>
                                            <a className="content__dele" href="">{iconTrash}Xóa</a>
                                            <Link target="_blank" to="/ed" className="content__edit" href="">{iconEdit}Sửa</Link>
                                        </div>
                                    </devicePixelRatio>
                                </div>
                            </div>
                        </div>
                        <div className="tab-pane fade" id="v-pills-addshop" role="tabpanel" aria-labelledby="v-pills-addshop-tab">
                            <AddShop />
                        </div>
                        <div className="tab-pane fade" id="v-pills-allcategory" role="tabpanel" aria-labelledby="v-pills-allcategory-tab">
                            <CategoryListHome/>
                        </div>
                        <div className="tab-pane fade" id="v-pills-addcategory" role="tabpanel" aria-labelledby="v-pills-addcategory-tab">
                            <AddCategory/>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    )
}

export default AdminManagement
