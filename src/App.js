import './App.css';
import React, { Component } from 'react';
import Login from './pages/Login';
import Header from './components/Header/Header'
import {userService} from './_services/user.service'
import {BrowserRouter, Switch, Route, Redirect,Router} from 'react-router-dom';
import { history } from './_helpers/history';
import Register from './pages/Register';
import toast, { Toaster } from 'react-hot-toast';
import Home from './pages/Home';
import Footer from './components/Footer/Footer'
import ProductDetail from './components/ProductDetail/ProductDetail';
import Cart from './components/Cart/Cart';
import AddProduct from './components/ProductRegister/AddProduct'
import ShopListHome from './containers/ShopListHome/ShopListHome'
import Vendor from './components/Vendor/Vendor'
import { useSelector } from 'react-redux';
import RoleBasedRouting from './components/RoleBaseRouting/RoleBaseRouting';
import Profile from './components/Profile/Profile';
import CategoryListHome from './containers/CategoryListHome/CategoryListHome'
import ShopManagement from './components/ShopManagement/ShopManagement';
import AdminManagement from './components/AdminManagement/AdminManagement';



function App() {
    const auth = useSelector(state => state.auth)
    const infoUser = JSON.parse(localStorage.getItem('infoUser'))
    let role = infoUser ? infoUser.role : auth.infoUser.role;

    const ProtectedRouter = ({ component: Component, ...rest }) => (
        <Route {...rest} render={(props) => (
            auth.isLogged === true
            ? <Component {...props} />
            : <Redirect to='/login' />
        )} 
        />
    )

    return(
        <div>                        
            <Router history={history}>
            <React.Fragment>
                <Header/>  
                            
                <Switch>
                    <Route exact path={'/'} render={() => {
                        return <Redirect to={'/products'}/>
                    }}/>
                    <Route exact path={'/products'} component={Home}/> 
                    <Route exact path={'/products/:id'} component={ProductDetail}/> 
                    {/* <ProtectedRouter exact path={'/cart'} component={Cart}/>  */}
                    <RoleBasedRouting exact path="/cart" component={Cart} roles={[role]} />

                    <Route exact path={'/home'} component={Home}/> 
                    <Route exact path={'/login'} component={Login}/> 
                    <Route exact path={'/register'} component={Register}/> 
                    <Route exact path={'/logout'} component={Login}/> 
                    <Route exact path={'/product/add'} component={AddProduct}/> 
                    <RoleBasedRouting exact path="/shops" component={ShopListHome} roles={['Admin']} />
                    {/* <Route exact path={'/shops'} component={ShopListHome}/>  */}
                    <Route exact path={'/vendor'} component={Vendor}/> 
                    <Route exact path={'/profile'} component={Profile}/> 
                    <Route exact path={'/shop/manage'} component={ShopManagement}/> 
                    <Route exact path={'/admin/manage'} component={AdminManagement}/> 
                    <Route exact path={'/list/cate'} component={CategoryListHome}/> 
                    <Route exact path={'/shops'} component={ShopListHome}/> 

                    {/* <Route exact path={'/order'} render={() => {
                        return <Redirect to={'/orderadress'}/>
                    }}/>
                    <Route exact path={'/contact'} component={Contact} />
                    <PrivateRoute exact path="/homepage" component={HomePage} />  

                    <Route exact path={'/products'} component={Home}/>      
                    <Route exact path={'/orderadress'} component={OrderAdress}/>              
                    <Route exact path={'/shop'} component={Shop}/>   
                                                    
                    <Route exact path={'/register'} component={Register}/>
                    
                    <Route exact path={'/products/:id'} component={ProductDetail}/> 
                                    
                    <Route exact patr={'/cart'} component={ShoppingCart} />  */}                    
                </Switch>                         
                <Footer/>
            </React.Fragment>
            </Router>

            <Toaster
                position="top-center"
                reverseOrder={false}
                gutter={8}
                containerClassName=""
                containerStyle={{}}
                toastOptions={{
                    className: '',
                    duration: 5000,
                    style: {
                    background: '#363636',
                    color: '#fff',
                    },
                    success: {
                    duration: 3000,
                    theme: {
                        primary: 'green',
                        secondary: 'black',
                    },
                    },
                }}
                />
        </div>
    )
}

export default App;
