import {
  authHeader
} from '../_helpers/auth-header';
import axios from 'axios';

export const userService = {
  login,
};

function login(username, password) {
  const requestOptions = {
    username,
    password
  };
  console.log(requestOptions)
  axios
    .post('https://pbl6-seafood.herokuapp.com/api/user/login/', requestOptions)
    .then(user => {
      //console.log(JSON.parse(atob(response.data.split('.')[1])).id)
      console.log(user);
      return user;
    })
    .catch(error => {

    });
}
// function logout() {
//     localStorage.removeItem('user');
// }

// function getAll() {
//     const requestOptions = {
//         method: 'GET',
//         headers: authHeader()
//     };

//     return fetch(`http://localhost:3000/users`, requestOptions).then(handleResponse);
// }

// function getById(id) {
//     const requestOptions = {
//         method: 'GET',
//         headers: authHeader()
//     };

//     return fetch(`http://localhost:3000/users/${id}`, requestOptions).then(handleResponse);
// }

// function register(user) {
//     const requestOptions = {
//         method: 'POST',
//         headers: { 'Content-Type': 'application/json' },
//         body: JSON.stringify(user)
//     };

//     return fetch(`http://localhost:3000/register`, requestOptions).then(handleResponse);
// }

// function update(user) {
//     const requestOptions = {
//         method: 'PUT',
//         headers: { ...authHeader(), 'Content-Type': 'application/json' },
//         body: JSON.stringify(user)
//     };

//     return fetch(`http://localhost:3000/users/${user.id}`, requestOptions).then(handleResponse);;
// }


// function _delete(id) {
//     const requestOptions = {
//         method: 'DELETE',
//         headers: authHeader()
//     };
//     return fetch(`http://localhost:3000/users/${id}`, requestOptions).then(handleResponse);
// }

// function handleResponse(response) {
//     return response.text().then(text => {
//         const data = text && JSON.parse(text);
//         if (!response.ok) {
//             if (response.status === 401) {
//                 // auto logout if 401 response returned from api
//                 logout();

//             }

//             const error = (data && data.message) || response.statusText;
//             return Promise.reject(error);
//         }

//         return data;
//     });
// }