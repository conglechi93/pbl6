import axios from 'axios'

const instance = axios.create({
    baseURL: 'https://pbl6-seafood.herokuapp.com/api',
});

const reqAxios = () => {
  const token = localStorage.getItem('user')
  instance.defaults.headers.common['Authorization'] = `Bearer ${token}`
  return instance
}

export default reqAxios;